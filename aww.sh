#!/bin/bash

curl -s https://complimentr.com/api > ~/aww.txt

jq -r '.compliment' ~/aww.txt > ~/aww

cat ~/aww | cowsay

rm ~/aww

rm ~/aww.txt